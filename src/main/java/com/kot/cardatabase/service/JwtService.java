package com.kot.cardatabase.service;

import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import org.springframework.http.HttpHeaders;

import java.util.Date;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

@Component
//public class JwtService {
//    static final long EXPIRATION_TIME = 86400000;
//    static final String PREFIX = "Bearer";
//
//    // Generate secret key. Only for demonstration purposes.
//    // In production, you should read it from the application configuration.
//    public SecretKey getSecretKey() {
//        try {
//            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
//            keyGenerator.init(256);
//            return keyGenerator.generateKey();
//        } catch (NoSuchAlgorithmException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    // Generate signed JWT token
//    public String getToken(String username) {
//        return Jwts.builder()
//                .claim("sub", username)
//                .expiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
//                .signWith(getSecretKey())
//                .compact();
//    }
//
//    // Get a token from request Authorization header,
//    // verify the token, and get username
//    public String getAuthUser(HttpServletRequest request) {
//        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
//
//        if (token != null) {
//            return Jwts.parser()
//                    .verifyWith(getSecretKey())
//                    .build()
//                    .parseSignedClaims(token.replace(PREFIX,""))
//                    .getPayload()
//                    .getSubject();
//        }
//        return null;
//    }
//}
public class JwtService {
    static final long EXPIRATIONTIME = 86400000; // 1 day in ms. Should be shorter in production.
    static final String PREFIX = "Bearer";

    // Generate secret key. Only for the demonstration purposes.
    // In production, you should read it from the application configuration.
    static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    // Generate signed JWT token
    public String getToken(String username) {
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME)).signWith(key).compact();

        return token;
    }

    // Get a token from request Authorization header,
    // verify thea token, and get username
    public String getAuthUser(HttpServletRequest request) {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (token != null) {
            String user = Jwts.parser().setSigningKey(key).build().parseClaimsJws(token.replace(PREFIX, ""))
                    .getBody().getSubject();

            if (user != null)
                return user;
        }
        return null;
    }
}
