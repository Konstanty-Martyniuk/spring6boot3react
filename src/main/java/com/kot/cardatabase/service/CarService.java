package com.kot.cardatabase.service;

import com.kot.cardatabase.domain.Car;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class CarService {
    @PreAuthorize("hasRole('USER')")
    public void updateCar(Car car) {
        //TODO
    }

    @PreAuthorize("hasRole('ADMIN')")
    public void deleteOwner(Car car) {
        //TODO
    }
}
