package com.kot.cardatabase.domain;

public record AccountCredentials(String username, String password) {
}
