package com.kot.cardatabase.web;

import com.kot.cardatabase.domain.Car;
import com.kot.cardatabase.domain.CarRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CarController {
    private final CarRepository repository;
    @GetMapping("/cars")
    public Iterable<Car> getCars() {
        return repository.findAll();
    }


}
