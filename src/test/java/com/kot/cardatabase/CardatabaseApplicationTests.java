package com.kot.cardatabase;

import com.kot.cardatabase.web.CarController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class CarDatabaseApplicationTests {

	@Autowired
	private CarController carController;
	@Test
	@DisplayName("Check if controller exists")
	void contextLoads() {
		assertThat(carController).isNotNull();
	}

}
