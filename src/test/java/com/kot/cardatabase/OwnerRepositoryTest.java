package com.kot.cardatabase;

import com.kot.cardatabase.domain.Owner;
import com.kot.cardatabase.domain.OwnerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class OwnerRepositoryTest {
    @Autowired
    private OwnerRepository repository;

    @Test
    void saveOwner() {
        repository.save(new Owner("Konstanty", "Kot"));
        assertThat(repository.findByFirstname("Konstanty").isPresent()).isTrue();
    }

    @Test
    void deleteOwners() {
        repository.save(new Owner("Hot", "Hotters"));
        repository.deleteAll();
        assertThat(repository.count()).isEqualTo(0);
    }
}
